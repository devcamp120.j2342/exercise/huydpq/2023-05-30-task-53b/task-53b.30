package com.devcamp.customerInvoice.models;

public class Invoice {
    private int id;
    private Customer customer;
    private double amount;
    public Invoice() {
    }
    public Invoice(int id, Customer customer, double amount) {
        this.id = id;
        this.customer = customer;
        this.amount = amount;
    }
    public int getId() {
        return id;
    }
    public Customer getCustomer() {
        return customer;
    }
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
    public double getAmount() {
        return amount;
    }
    public void setAmount(double amount) {
        this.amount = amount;
    }
    
    public int getCustomerID(){
        return this.customer.getId();
    }
    public String getCustomerName(){
        return this.customer.getName();
    }

    public double getCustomerDiscount(){
        return this.customer.getDiscount();
    }
    
    public double getAmountAfterDiscount() {
        return this.getAmount() - (this.getAmount()* this.customer.getDiscount()/100);
    }
    @Override
    public String toString() {
        return "Invoice [id=" + id + ","+ this.customer + ", amount= " + amount + ", AmountAfterDiscount= " + getAmountAfterDiscount()+" ]";
    }

    
    
    

}
