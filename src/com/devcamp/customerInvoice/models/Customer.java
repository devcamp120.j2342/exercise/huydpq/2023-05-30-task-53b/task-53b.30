package com.devcamp.customerInvoice.models;

public class Customer {
    private String name;
    private int id;
    private int discount;

    public Customer(){

    }

    public Customer(String name, int id, int discount) {
        this.name = name;
        this.id = id;
        this.discount = discount;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    @Override
    public String toString() {
        return "Customer [name= " + name + ", id= " + id + ", discount= " + discount + " %]";
    }

    
    
    

}
