import com.devcamp.customerInvoice.models.Customer;
import com.devcamp.customerInvoice.models.Invoice;

public class App {
    public static void main(String[] args) throws Exception {
        Customer customer1 = new Customer();
        Customer customer2 = new Customer("Anh Luong", 3, 10);
        System.out.println("Customer 1: ");
        System.out.println(  customer1);
        System.out.println("Customer 2: ");
        System.out.println( customer2);

        Invoice invoice1 = new Invoice(1, customer1, 200.000);
        Invoice invoice2 = new Invoice(2, customer2, 300.000 );
        System.out.println("Invoice 1: ");
         System.out.println( invoice1);
         System.out.println("Invoice 2: ");
        System.out.println(invoice2);

    }
}
